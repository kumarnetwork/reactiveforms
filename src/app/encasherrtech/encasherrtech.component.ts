import { MockCatagories } from './catagory.model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, } from '@angular/forms';
import { ValidateDropdown } from './custom.validator';

@Component({
  selector: 'app-encasherrtech',
  templateUrl: './encasherrtech.component.html',
  styleUrls: ['./encasherrtech.component.css']
})
export class EncasherrtechComponent implements OnInit {
  rForm: FormGroup;
  catagories: any[];
  submitValue: any;

  titleErrorMessage: string;
  descriptionErrorMessage: string;
  catagoryErrorMessage: string;
  constructor(private formBuilder: FormBuilder) {
    this.populateCatagories();
    this.initializeErrorMessages();
    this.rForm = formBuilder.group({
      "title": [null, Validators.required],
      "description": [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(30)])],
      "catagory": [0, ValidateDropdown]
    });
   }

  ngOnInit() {
  }

  populateCatagories(){
    this.catagories = MockCatagories;
  }

  initializeErrorMessages(){
    this.titleErrorMessage = "This title is required";
    this.descriptionErrorMessage = "This description is required";
    this.catagoryErrorMessage = "This catagory is required";
  }
  submitDetails(value){
    this.submitValue = value;
  }
}
