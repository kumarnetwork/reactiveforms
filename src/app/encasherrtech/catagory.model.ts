export interface ICatagory {
    id: number;
    name: string;
    description: string;
}

export const MockCatagories: ICatagory[] = [
    {
        id: 0,
        name: "Please Select",
        description: "Default Value"
    },
    {
        id: 1,
        name: "Electronics",
        description: "All Electronics devices"
    },
    {
        id: 2,
        name: "Home Appliance",
        description: "All Home Appliances"
    },
    {
        id: 3,
        name: "Fashion",
        description: "All Fashion store"
    },
    {
        id: 4,
        name: "Kindle",
        description: "All Kindle store"
    }
]