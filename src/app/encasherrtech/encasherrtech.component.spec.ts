import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EncasherrtechComponent } from './encasherrtech.component';

describe('EncasherrtechComponent', () => {
  let component: EncasherrtechComponent;
  let fixture: ComponentFixture<EncasherrtechComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EncasherrtechComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EncasherrtechComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
